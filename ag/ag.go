package ag

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/labstack/echo"
	"go.uber.org/ratelimit"
	"gopkg.in/eapache/queue.v1"
)

// QoS types
const (
	PRIORITY    = iota
	SENSITIVE   = iota
	INSENSITIVE = iota
)

type register struct {
	url  string
	rate int
}

type clients struct {
	url   string
	rate  int
	topic string
}

type response struct {
	to      []string `json:"to"`
	topic   string   `json:"topic"`
	payload string   `json:"payload"`
}

type sublistReq struct {
	Host string `json:"host"`
	Rate int    `json:"rate"`
}

type bufferStateReq struct {
	AgID string `json:"agID"`
	QOS1 int    `json:"qos1"`
	QOS2 int    `json:"qos2"`
	QOS3 int    `json:"qos3"`
}

type AgClientByQueue struct {
	AgID string `json:"agID"`
	Qtdi int    `json:"qtdi"`
	Qtds int    `json:"qtds"`
	Qtdp int    `json:"qtdp"`
}

// SubRequest is the structure for http subscription requests
type SubRequest struct {
	Topic string `json:"topic"`
	To    string `json:"to"`
	Rate  int    `json:"rate"`
}

type SensorRequest struct {
	Topic   string          `json:"topic"`
	Payload json.RawMessage `json:"payload"`
	Rate    int             `json:"rate"`
}

var buffer = map[int]*queue.Queue{
	INSENSITIVE: queue.New(),
	SENSITIVE:   queue.New(),
	PRIORITY:    queue.New(),
}

var subsByQueue = map[int]*queue.Queue{
	INSENSITIVE: queue.New(),
	SENSITIVE:   queue.New(),
	PRIORITY:    queue.New(),
}

var subsByTopic = map[string][]register{}
var subsByClientAddr = map[string]register{}

//var subsByQueue = map[int]clients{}

var ArcaIP = "http://localhost:8080"
var AgID = "http://localhost:8080"

var bufferRT = ratelimit.New(1) // per second

// SubListener listens to http subscription requests
func SubListener(c echo.Context) error {

	req := SubRequest{}
	err := c.Bind(&req)

	fmt.Println("subscribing", req, err)
	if err != nil {
		return err
	}

	subscribe(req)

	return c.JSON(http.StatusOK, "Hello World")
}

// subscribe subscribes a client to a topic, with the selected QOS
func subscribe(req SubRequest) {
	c := clients{req.To, req.Rate, req.Topic}
	r := register{req.To, req.Rate}
	subsByTopic[req.Topic] = append(
		subsByTopic[req.Topic],
		r,
	)

	subsByClientAddr[req.To] = r

	if req.Rate <= 1024 {
		subsByQueue[PRIORITY].Add(c)
	} else if req.Rate > 1024 && req.Rate <= 2048 {
		subsByQueue[SENSITIVE].Add(c)
	} else {
		subsByQueue[INSENSITIVE].Add(c)
	}

	sendClientsByQueue([]int{
		subsByQueue[INSENSITIVE].Length(),
		subsByQueue[SENSITIVE].Length(),
		subsByQueue[PRIORITY].Length(),
	})
	// update subscriber list in "arcabouço"
	sendSubList(subsByClientAddr)
//	SetSubQueue(INSENSITIVE, int64(subsByQueue[INSENSITIVE].Length()))
//	SetSubQueue(SENSITIVE, int64(subsByQueue[SENSITIVE].Length()))
//	SetSubQueue(PRIORITY, int64(subsByQueue[PRIORITY].Length()))
}

//Publish pusblishes a topic and payload
func Publish(topic string, payload string) {
	go processPublished(topic, payload)
}

// processPublished is called when a topic is published, and distributes the message between QoS buffers
func processPublished(topic string, payload string) {

	subs := subsByTopic[topic]
	isen := response{[]string{}, topic, payload}
	sen := response{[]string{}, topic, payload}
	pri := response{[]string{}, topic, payload}
//	fmt.Println("out-agr", subs)

	for _, s := range subs {
		if s.rate <= 1024 {
			pri.to = append(pri.to, s.url)
		
		} else if s.rate > 1024 && s.rate <= 2048 {
			sen.to = append(sen.to, s.url)
		
		} else {
		
			isen.to = append(isen.to, s.url)
		}
	}

	if len(isen.to) > 0 {
		buffer[INSENSITIVE].Add(isen)

	}

	if len(sen.to) > 0 {
		buffer[SENSITIVE].Add(sen)

	}

	if len(pri.to) > 0 {
		buffer[PRIORITY].Add(pri)

	}

	// update
	//fmt.Println("out-agr", buffer[INSENSITIVE].Length(), buffer[SENSITIVE].Length(), buffer[PRIORITY].Length())
	sendQoS([]int{
		buffer[INSENSITIVE].Length(),
		buffer[SENSITIVE].Length(),
		buffer[PRIORITY].Length(),
	})
}

func sendSubList(subs map[string]register) {
	s := []sublistReq{}
	for _, r := range subs {
		s = append(s, sublistReq{
			Host: r.url,
			Rate: r.rate,
		})
	}
	d := struct {
		AgID        string       `json:"agID"`
		Subscribers []sublistReq `json:"subscribers"`
	}{AgID: AgID, Subscribers: s}
	b, err := json.Marshal(d)
	if err != nil {
		fmt.Println("cannot send subscribers list", err, subs)
	}
	err = Post(ArcaIP+"/api/orquestrator/subscribers", b)
	if err != nil {
		fmt.Println("cannot send subscribers list", "post error", err, subs)
	}
}

func sendQoS(qos []int) {
	bufferRT.Take()

	if len(qos) != 3 {
		qos = []int{0, 0, 0}
	}

	d := bufferStateReq{
		AgID: AgID,
		QOS1: qos[0],
		QOS2: qos[1],
		QOS3: qos[2],
	}

	b, err := json.Marshal(d)
	if err != nil {
		fmt.Println("cannot send buffer size", err, qos)
	}

	err = Post(ArcaIP+"/api/orquestrator/qos-buffers", b)
	if err != nil {
		fmt.Println("cannot send buffer size", "post error", err, qos)
	}
}

func sendClientsByQueue(qtd []int) {
	bufferRT.Take()

	d := AgClientByQueue{
		AgID: AgID,
		Qtdi: qtd[0],
		Qtds: qtd[1],
		Qtdp: qtd[2],
	}

	b, err := json.Marshal(d)
	if err != nil {
		fmt.Println("cannot send buffer size", err, qtd)
	}

	err = Post(ArcaIP+"/api/orquestrator/clients-queue", b)
	if err != nil {
		fmt.Println("cannot send buffer size", "post error", err, qtd)
	}
}
