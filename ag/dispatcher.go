package ag

import (
	"bytes"
	"fmt"
	"net/http"
	"strconv"
	"time"
	"encoding/json"
	"io/ioutil"
	//"golang.org/x/time/rate.Limiter"
	//"github.com/juju/ratelimit"
	"gopkg.in/eapache/queue.v1"
)

// ops/second
var effort = map[int]float64{
	INSENSITIVE: 20833,
	SENSITIVE:   41666,
	PRIORITY:    62500,
}

var maptime = map[int]int64{
	INSENSITIVE: 1536,
	SENSITIVE:   1024,
	PRIORITY:    512,
}

type FileData struct {
	Packages int `json:"packages"`
	Size int64    `json:"size"`
	Queue int    `json:"fila"`
	Quantidade int `json:"Quantidate"`
}


func SetEffort(qos int, eff float64) {
	effort[qos] = eff
}

//func SetSubQueue(q int, n int64) {
//	SubQueue[q] = n
//}

func Dispatcher(id string) error {
	go dispatch(id, buffer[INSENSITIVE], INSENSITIVE, maptime[INSENSITIVE])
	go dispatch(id, buffer[SENSITIVE], SENSITIVE, maptime[SENSITIVE])
	go dispatch(id, buffer[PRIORITY], PRIORITY, maptime[PRIORITY])
	return nil

}

func dispatch(id string, q *queue.Queue, effortuni int, timeout int64) {
		gravacao :=  time.Now()
		pacotes := 0
		batch := int64(0)
		quantasVezes := 0
		for now := range time.Tick(time.Millisecond * time.Duration(timeout)) {
			quantasVezes++
			init := now
			i := float64(10)
			if(effort[effortuni] != 0){
				i = effort[effortuni]
			}
			//rl := ratelimit.NewBucketWithRate(i, int64(i))
			requisicoes := int(i/8)
			rateLimit := time.Second/time.Duration(requisicoes)
			throttle := time.Tick(rateLimit)

			for {
				start := time.Now()
				diff := start.Sub(init).Milliseconds()
				
				if(diff >=500){
					fmt.Println("out-agr Quantidade?",q.Length(), effortuni )
					break;	
				}
				if(q.Length() == 0){
					continue
				}
				item := q.Peek()
				q.Remove()

				resp, ok := item.(response)
				if !ok {
					fmt.Println("Wrong response type in queue", now)
					continue
				}
				size := int64(len(resp.payload))
				
				for _, url := range resp.to {
						<-throttle
						queue := strconv.Itoa(effortuni)
						err := Post(url,
							[]byte("{\"queue\": \""+queue+"\", \"payload\":\""+resp.payload+"\", \"topic\":\""+resp.topic+"\", \"to\":\""+id+"\"}"),
						)
						if err != nil {
							fmt.Println("Cannot send to", url, ":", err)
						}
						batch = batch + size
						pacotes++
				}
			}
		//	fmt.Println("out-agr saiu ", q.Length())
			if(q.Length() > 0){
				for i := 0; i < q.Length(); i++ {
				   
					q.Remove()
				} 
			}
			
		//	fmt.Println("out-agr saiu ??")
			box := FileData{
				Packages:  pacotes,
				Size:  batch,
				Queue: effortuni,
				Quantidade: quantasVezes,
			}
			start := time.Now()
			diff := start.Sub(gravacao).Minutes()
			if(diff>=1){
				pagesJson,_ := json.Marshal(box);
				_ = ioutil.WriteFile(start.Format("Jan _2 15:04:05.000") + ".json", pagesJson, 0644);
				pacotes= 0
				batch =int64(0)
				gravacao = time.Now()
				quantasVezes = 0
			}
		}
}

// Post send a json message via http post
func Post(url string, j []byte) error {

	req, err := http.NewRequest("POST", url, bytes.NewBuffer(j))
	req.Header.Set("Content-Type", "application/json")

	client := &http.Client{}
	resp, err := client.Do(req)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}
