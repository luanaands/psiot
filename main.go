package main

import (
	"fmt"
	"net/http"

	"./ag"
	"./client"
	"github.com/surgemq/surgemq/service"
	"gopkg.in/alecthomas/kingpin.v2"

	//"time"

	"log"
	"net"

	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
)

var (
	arcaIP = kingpin.Flag("arca", "IP do Arcabouço.").
		Short('a').
		Required().
		String()
	publishers = kingpin.Flag("publishers", "Json com publicadores: [{topic:string, payload:any, rate:int}].").
			Short('j').
			Default(`
			[
				{"topic": "iot", "payload": "50s", "rate": 1024},
				{"topic": "cauldron", "payload": "40", "rate": 1024},
				{"topic": "tts", "payload": "40", "rate": 1024}
			]
			`).
		String()
	arcaID = kingpin.Flag("id", "id do ag.").
		Short('i').
		Default("ag123").
		String()
	port = kingpin.Flag("porta", "porta do Arcabouço.").
		Short('p').
		Default(":8088").
		String()
)

func main() {
	kingpin.Version("0.0.1")
	kingpin.Parse()
	ag.ArcaIP = *arcaIP
	ag.AgID = *arcaID

	fmt.Println("out-agr starting")

	go ag.Dispatcher(ag.AgID)

	// subscribe to orquestrator changes
	ip := GetOutboundIP()
	returnURL := "http://" + ip.String() + *port + "/orquestrator"
	ag.Post(
		*arcaIP+"/api/orquestrator/subscribe",
		[]byte("{\"agID\":\""+ag.AgID+"\",\"effortReturnURL\": \""+returnURL+"\"}"),
	)

	// Echo instance
	e := echo.New()
	e.Use(mw.Recover())
	e.Use(mw.Logger())

	/// CORS restricted
	// Allows requests from all origins
	// wth GET, PUT, POST or DELETE method.
	e.Use(mw.CORSWithConfig(mw.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))
    
	e.POST("/", h)
	e.POST("/orquestrator", effortListener)
	e.POST("/subscribe", ag.SubListener)
	e.POST("/sensor", SensorListener)

	e.Logger.Fatal(e.Start(*port))
}

func SensorListener(c echo.Context) error {

	req := client.PublisherType{}
	err := c.Bind(&req)

	fmt.Println("Recebendo dados", req.Topic, req, err)
	if err != nil {
		return err
	}

	go client.Publisher(req, ag.Publish)
	return c.JSON(http.StatusOK, "Hello World")
}

func h(c echo.Context) (err error) {
	return c.JSON(http.StatusOK, "Hello World")
}

// effortListener listens to changes in effort for each QoS channel (buffer)
func effortListener(c echo.Context) (err error) {
	var req []float64

	err = c.Bind(&req)
	if err != nil {
		fmt.Println("out-agr error receiving priority:", err)
	}

	if len(req) > 2 {
		ag.SetEffort(ag.INSENSITIVE, req[ag.INSENSITIVE])
		ag.SetEffort(ag.SENSITIVE, req[ag.SENSITIVE])
		ag.SetEffort(ag.PRIORITY, req[ag.PRIORITY])

	}
	fmt.Println("received", req)

	return c.JSON(http.StatusOK, "Hello World")
}

func mqttServer(url string) *service.Server {
	svr := &service.Server{
		KeepAlive:        3000,          // seconds
		ConnectTimeout:   400,           // seconds
		SessionsProvider: "mem",         // keeps sessions in memory
		Authenticator:    "mockSuccess", // always succeed
		TopicsProvider:   "mem",         // keeps topic subscriptions in memory
	}

	// Listen and serve connections at localhost:1883
	return svr
}

// GetOutboundIP gets the preferred outbound ip of this machine
func GetOutboundIP() net.IP {
	conn, err := net.Dial("udp", "10.0.0.1:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}
