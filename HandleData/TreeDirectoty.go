package handle

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"../ag"
)

func lerlinha(oi string, texto string) ([]string, error) {
	//abre o arquivo

	arquivo, err := os.Open(oi)
	// Caso tenha encontrado algum erro ao tentar abrir o arquivo retorne o erro encontrado
	if err != nil {
		return nil, err
	}

	// Garante que o arquivo sera fechado apos o uso
	defer arquivo.Close()

	// Cria um scanner que le cada linha do arquivo
	var linha []string
	scanner := bufio.NewScanner(arquivo)
	for scanner.Scan() {

		if strings.Contains(scanner.Text(), texto) {
			linha = append(linha, scanner.Text())

			//log.Fatal("quantas")
			//fmt.Println(linha)
		}
	}

	// Retorna as linhas lidas e um erro se ocorrer algum erro no scanner
	return linha, scanner.Err()
}

func lerLinhaPublish(oi string, texto string, addr string) error {
	//abre o arquivo

	arquivo, err := os.Open(oi)
	// Caso tenha encontrado algum erro ao tentar abrir o arquivo retorne o erro encontrado
	if err != nil {
		return err
	}
	// Garante que o arquivo sera fechado apos o uso
	defer arquivo.Close()

	// Cria um scanner que le cada linha do arquivo
	//var linha []string
	scanner := bufio.NewScanner(arquivo)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), texto) {
			//linha = append(linha, scanner.Text())
			fmt.Println(scanner.Text())
			value := TrataData(scanner.Text(), texto)
			SendData(value, addr)
		}
	}
	// Retorna um erro se ocorrer algum erro no scanner
	return scanner.Err()
}

func writetofile(filename string, datas []string) error {
	//abre o arquivo

	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
		return err
	}

	datawriter := bufio.NewWriter(file)

	for _, data := range datas {
		_, _ = datawriter.WriteString(data + "\n")
		fmt.Println(data)
	}

	datawriter.Flush()
	file.Close()
	return file.Sync()
}

func pecorrerCaminho(TypeData string, addr string) {
///home/gabi/dados/physionet.org/files/mimicdb/1.0.0/414/41400001.txt
	err := filepath.Walk("../../../dados/physionet.org/files/mimicdb/1.0.0/414/",
		// funcao para pecorrer o diretório
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if strings.Contains(path, ".txt") {
				fmt.Println(path)
				// ler linha e manda
				err := lerLinhaPublish(path, TypeData, addr)
				if err != nil {
					log.Println(err)
				}
			}
			return nil
		})
	if err != nil {
		log.Println(err)
	}

}

func TrataData(text string, types string) string {
	valor := strings.Replace(text, types, "", -1)
	switch types {
	case "ABP":
		valor = strings.Replace(valor, "\t", " ", -1)
		valor = strings.TrimSpace(valor)
		break
	case "PULSE":
		valor = strings.Replace(valor, "\t", " ", -1)
		valor = strings.TrimSpace(valor)
		break
	case "SpO2":
		valor = strings.Replace(valor, "\t", " ", -1)
		valor = strings.TrimSpace(valor)
		break
	}
	return valor
}

func SendData(valor string, addr string) error {

	
	limiter := time.Tick(time.Millisecond * 1024)
	<-limiter
	stri := `{"topic" : "abp", "payload": "` + valor + `", "rate": 400}`
	err := ag.Post(
		addr,
		[]byte(stri),
	)
	start := time.Now()
	fmt.Println("Sensor send", start)
//	fmt.Println("Valor:", valor , "tamanho :", int64(len(valor)));
	return err
}
