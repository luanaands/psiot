package main

import (
	"fmt"

	"./dados"
	"gopkg.in/alecthomas/kingpin.v2"
)

var (
	address = kingpin.Flag("address", "Endereço do agregador.").
		Short('a').
		Required().
		String()
	porta = kingpin.Flag("porta", "porta do agregador.").
		Short('p').
		Default(":8088").
		String()
	tipo = kingpin.Flag("tipo", "Tipo do dado").
		Short('t').
		Default("ABP").
		String()
)

//	function init
func main() {

	kingpin.Version("0.0.1")
	kingpin.Parse()
	//	data := []*client.PublisherType{}
	//	typedata, err := strconv.Atoi(*tipo)
	//	if err != nil {
	//		fmt.Println("Convert int: ", err)
	//	}
	//	fmt.Println(typedata)
	///	switch typedata {
	//	case 0:
	//		data = dados.HandleDataAbp()
	//		break
	//	case 1:
	//		data = dados.HandleDataSp02()
	//		break
	//	}

	addr := "http://" + *address + *porta + "/sensor"

	err := dados.Path(*tipo, addr)
	if err != nil {
		fmt.Println("error send data sensor: ", err)
	}
	//limiter := time.Tick(time.Millisecond * 1024)
	//for _, d := range data {

	//	b, err := json.Marshal(d)
	//	if err != nil {
	//		fmt.Println("cannot data sensor: ", err)
	//	}
	//
	//		<-limiter
	//		err = ag.Post(
	//			addr,
	//			[]byte(b),
	//		)
	//		start := time.Now()
	//		fmt.Println("Sensor send", start)
	//		if err != nil {
	//			fmt.Println("error send data sensor: ", err)
	//		}
	//	}

}
