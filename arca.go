package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net"
	"net/http"
	"time"

	"./ag"
	"gopkg.in/alecthomas/kingpin.v2"

	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
)

var (
	arcaPort = kingpin.Flag("port", "Porta para rodar arcabouço.").
		Short('p').
		Required().
		String()
)

// QOS effort
type QOS int

// QOS effort
const (
	QOS1 QOS = iota
	QOS2
	QOS3
)

type agSubscriber struct {
	Host string `json:"host"`
	QOS  QOS    `json:"qos"`
}

type agSubscription struct {
	AgID            string `json:"agID"`
	EffortReturnURL string `json:"effortReturnURL"`
}

type agBufferState struct {
	AgID string `json:"agID"`
	QOS1 int64  `json:"qos1"`
	QOS2 int64  `json:"qos2"`
	QOS3 int64  `json:"qos3"`
}

type agClientsState struct {
	AgID string `json:"agID"`
	QTDi int64  `json:"qtdi"`
	QTDs int64  `json:"qtds"`
	QTDp int64  `json:"qtdp"`
}

var subscribedAggregators = map[string]agSubscription{}
var aggregatorSubscribers = map[string][]agSubscriber{}
var aggregatorBufferSize = map[string]agBufferState{}
var aggregatorClientsSize = map[string]agClientsState{}

func main() {
	kingpin.Version("0.0.1")
	kingpin.Parse()

	// subscribe to orquestrator changes
	_ = GetOutboundIP()

	// start orquestrator
	go func() {
		for now := range time.Tick(time.Second * 10) {
			orchestratorBrains(now)
		}
	}()

	// Echo instance
	e := echo.New()
	e.Use(mw.Recover())
	e.Use(mw.Logger())

	/// CORS restricted
	// Allows requests from all origins
	// wth GET, PUT, POST or DELETE method.
	e.Use(mw.CORSWithConfig(mw.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	e.POST("/", h)
	e.POST("/api/orquestrator/subscribe", orqSubscribeListener)
	e.POST("/api/orquestrator/subscribers", subscriberListener)
	e.POST("/api/orquestrator/qos-buffers", bufferListener)
	e.POST("/api/orquestrator/clients-queue", AgClientsListener)

	e.Logger.Fatal(e.Start(":" + *arcaPort))
}

func h(c echo.Context) (err error) {
	return c.JSON(http.StatusOK, "Hello World")
}

// orqSubscribeListener listens to aggregators subscribing to the orquestrator
func orqSubscribeListener(c echo.Context) (err error) {
	req := agSubscription{}
	err = c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Error string `json:"error"`
		}{Error: "error parsing subscription:" + err.Error()})
	}

	err = createSubscription(req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Error string `json:"error"`
		}{Error: "error subscribing to orquestrator:" + err.Error()})
	}

	return c.JSON(http.StatusOK, struct {
		Message string `json:"message"`
	}{Message: "Subscribed successfully"})
}

// subscriberListener listens to changes in aggregator's subscribers
func subscriberListener(c echo.Context) (err error) {
	req := struct {
		AgID        string         `json:"agID"`
		Subscribers []agSubscriber `json:"subscribers"`
	}{}
	err = c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Error string `json:"error"`
		}{Error: "error parsing subscribers:" + err.Error()})
	}

	err = setAggregatorSubscribers(req.AgID, req.Subscribers)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Error string `json:"error"`
		}{Error: "error setting subscribers:" + err.Error()})
	}

	return c.JSON(http.StatusOK, struct {
		Message string `json:"message"`
	}{Message: "Update successfull"})
}

// bufferListener listens to changes in buffer for each aggregator
func bufferListener(c echo.Context) (err error) {
	req := agBufferState{}
	err = c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Error string `json:"error"`
		}{Error: "error parsing buffer data:" + err.Error()})
	}

	err = setAggregatorBufferState(req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Error string `json:"error"`
		}{Error: "error setting buffer state:" + err.Error()})
	}

	return c.JSON(http.StatusOK, struct {
		Message string `json:"message"`
	}{Message: "Update successfull"})
}

func AgClientsListener(c echo.Context) (err error) {
	req := agClientsState{}
	err = c.Bind(&req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Error string `json:"error"`
		}{Error: "error parsing buffer data:" + err.Error()})
	}

	err = setAggregatorClientsSize(req)
	if err != nil {
		return c.JSON(http.StatusBadRequest, struct {
			Error string `json:"error"`
		}{Error: "error setting buffer state:" + err.Error()})
	}

	return c.JSON(http.StatusOK, struct {
		Message string `json:"message"`
	}{Message: "Update successfull"})
}

//func SendData(valor string, addr string) {

//	limiter := time.Tick(time.Millisecond * 1024)
//	<-limiter
//	stri := `{"topic" : "abp", "payload": "` + valor + `", "rate": 400}`
//	fmt.Println("Dados json", stri)
//	err := ag.Post(
//		addr,
//		[]byte(stri),
//	)
//	start := time.Now()
//	fmt.Println("Sensor send", start)
//	if err != nil {
//		fmt.Println("error send data sensor: ", err)
//	}
//}

// GetOutboundIP gets the preferred outbound ip of this machine
func GetOutboundIP() net.IP {
	conn, err := net.Dial("udp", "10.0.0.1:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}

func createSubscription(sub agSubscription) error {
	subscribedAggregators[sub.AgID] = sub
	return nil
}

func setAggregatorSubscribers(agID string, subs []agSubscriber) error {
	aggregatorSubscribers[agID] = subs
	return nil
}

func setAggregatorBufferState(buff agBufferState) error {
	aggregatorBufferSize[buff.AgID] = buff
	return nil
}

func setAggregatorClientsSize(buff agClientsState) error {
	aggregatorClientsSize[buff.AgID] = buff
	return nil
}

func sendEffort(effort []float64, url string) {

	b, err := json.Marshal(effort)
	if err != nil {
		fmt.Println("cannot send buffer size", err, effort)
	}

	err = ag.Post(url, b)
	if err != nil {
		fmt.Println("ag-out cannot send buffer size", "post error", err, effort)
	}
}

func orchestratorBrains(t time.Time) {
	fmt.Println("ag-out thinking brains")
	fmt.Println("ag-out subscribedAggregators", subscribedAggregators)
	fmt.Println("ag-out aggregatorSubscribers", aggregatorSubscribers)
	fmt.Println("ag-out aggregatorBufferSize", aggregatorBufferSize)
	fmt.Println("ag-out aggregatorClientsSize", aggregatorClientsSize)

	cal := float64(0) 
	// 20 kbps
// total := float64(2500)
	//10 kbps
//	total := float64(1250)
	// 5 kbps
	total := float64(625)
	// total := float64(50)
	for a, b := range aggregatorClientsSize {

		fmt.Println("a:", a, "b:", b)
		cal = cal + ((float64(b.QTDi) * 1) + (float64(b.QTDs) * 2) + (float64(b.QTDp) * 3))

	}
	valoDeI := float64(0)
	if cal > 0 {
		valoDeI = float64(total / cal)
		fmt.Println("ag-out valor: ", valoDeI, cal)
	}
	if valoDeI > 0 {
		i := float64(0)
		s := float64(0)
		p := float64(0)
		for a, b := range aggregatorClientsSize {
			i = float64(b.QTDi) * valoDeI
			s = float64(b.QTDs) * (valoDeI * 2)
			p = float64(b.QTDp) * (valoDeI * 3)

			d := []float64{p, s, i}

			fmt.Println("ag-out a", a, "filas: ", p, s, i)
			agSus := agSubscription{}
			agSus = subscribedAggregators[a]
			sendEffort(d, agSus.EffortReturnURL)
		}
	}

}
