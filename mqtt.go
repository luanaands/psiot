package main

import (
	"fmt"
	"github.com/surgemq/surgemq/service"
)

func main() {
	fmt.Println("starting")

	svr := &service.Server{
		KeepAlive:        3000,          // seconds
		ConnectTimeout:   400,           // seconds
		SessionsProvider: "mem",         // keeps sessions in memory
		Authenticator:    "mockSuccess", // always succeed
		TopicsProvider:   "mem",         // keeps topic subscriptions in memory
	}

	// Listen and serve connections at localhost:1883
	err := svr.ListenAndServe("tcp://localhost:1883")
	fmt.Println("terminating mqtt connection", err)
}
