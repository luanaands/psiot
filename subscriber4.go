package main

import (
	"fmt"
	"net/http"

	"./ag"
	"gopkg.in/alecthomas/kingpin.v2"

	"encoding/json"
	"log"
	"net"
	"strconv"
	"time"

	"github.com/labstack/echo"
	mw "github.com/labstack/echo/middleware"
)

var (
	subscriptions = kingpin.Flag("subscriptions", "Json com topicos para subscrição: [{url: string, topic:string, qos:int}].").
		Short('s').
		Default(`
			[
				{"topic": "spO2", "url": "http://localhost:8086/subscribe", "rate": 1024},
				{"topic": "abp", "url": "http://localhost:8086/subscribe", "rate": 1024},
				{"topic": "pulse", "url": "http://localhost:8086/subscribe", "rate": 1024}
				
			]
			`).
		String()
)

type sublist struct {
	URL   string `json:"url"`
	Topic string `json:"topic"`
	Rate  int    `json:"rate"`
}

func main() {
	kingpin.Version("0.0.1")
	kingpin.Parse()

	sublist := []sublist{}
	err := json.Unmarshal([]byte(*subscriptions), &sublist)
	if err != nil {
		log.Fatal("json error: ", err)
	}

	fmt.Println("client-out starting", sublist)

	// subscribe to orquestrator changes
	ip := GetOutboundIP()
	returnURL := "http://" + ip.String() + ":8089/subscribe-return"

	for _, s := range sublist {
		rate := strconv.Itoa(s.Rate)
		err := ag.Post(
			s.URL,
			[]byte("{\"to\": \""+returnURL+"\", \"rate\": "+rate+", \"topic\":\""+s.Topic+"\"}"),
		)
		if err != nil {
			fmt.Println("error subscribing with", returnURL, rate, err)
		}
	}

	// Echo instance
	e := echo.New()
	e.Use(mw.Recover())
	e.Use(mw.Logger())

	/// CORS restricted
	// Allows requests from all origins
	// wth GET, PUT, POST or DELETE method.
	e.Use(mw.CORSWithConfig(mw.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.PUT, echo.POST, echo.DELETE},
	}))

	e.POST("/", h)
	e.POST("/subscribe-return", listener)

	e.Logger.Fatal(e.Start(":8089"))
}

func h(c echo.Context) (err error) {
	return c.JSON(http.StatusOK, "Hello World")
}

// listener listens to changes in effort for each QoS channel (buffer)
func listener(c echo.Context) (err error) {
	start := time.Now()
	var req string

	err = c.Bind(&req)
	if err != nil {
		fmt.Println("error receiving subscription:", err)
	}
	fmt.Println("client-out received", req, start.Format("Jan _2 15:04:05"))

	return nil
}

// GetOutboundIP gets the preferred outbound ip of this machine
func GetOutboundIP() net.IP {
	conn, err := net.Dial("udp", "10.0.0.2:80")
	if err != nil {
		log.Fatal(err)
	}
	defer conn.Close()

	localAddr := conn.LocalAddr().(*net.UDPAddr)

	return localAddr.IP
}
