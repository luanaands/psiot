package dados

import (
	"bufio"
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
	"strings"
	"time"

	"../ag"
	"../client"
)

func lerlinha(oi string, texto string) ([]string, error) {
	//abre o arquivo

	arquivo, err := os.Open(oi)
	// Caso tenha encontrado algum erro ao tentar abrir o arquivo retorne o erro encontrado
	if err != nil {
		return nil, err
	}

	// Garante que o arquivo sera fechado apos o uso
	defer arquivo.Close()

	// Cria um scanner que le cada linha do arquivo
	var linha []string
	scanner := bufio.NewScanner(arquivo)
	for scanner.Scan() {

		if strings.Contains(scanner.Text(), texto) {
			linha = append(linha, scanner.Text())

			//log.Fatal("quantas")
			//fmt.Println(linha)
		}
	}

	// Retorna as linhas lidas e um erro se ocorrer algum erro no scanner
	return linha, scanner.Err()
}

func lerLinhaPublish(path string, texto string, addr string) error {
	//abre o arquivo

	arquivo, err := os.Open(path)
	// Caso tenha encontrado algum erro ao tentar abrir o arquivo retorne o erro encontrado
	if err != nil {
		return err
	}
	// Garante que o arquivo sera fechado apos o uso
	defer arquivo.Close()

	// Cria um scanner que le cada linha do arquivo
	//var linha []string
	scanner := bufio.NewScanner(arquivo)
	for scanner.Scan() {
		if strings.Contains(scanner.Text(), texto) {
			//linha = append(linha, scanner.Text())
			fmt.Println(scanner.Text())
			value := TrataData(scanner.Text(), texto)
			SendData(value, addr, texto)
		}
	}
	// Retorna um erro se ocorrer algum erro no scanner
	return scanner.Err()
}

func writetofile(filename string, datas []string) error {
	//abre o arquivo

	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
		return err
	}

	datawriter := bufio.NewWriter(file)

	for _, data := range datas {
		_, _ = datawriter.WriteString(data + "\n")
		fmt.Println(data)
	}

	datawriter.Flush()
	file.Close()
	return file.Sync()
}

func Path(TypeData string, addr string) error {

	err := filepath.Walk("../../dados/physionet.org/files/mimicdb/1.0.0/414/",
		// funcao para pecorrer o diretório
		func(path string, info os.FileInfo, err error) error {
			if err != nil {
				return err
			}
			if strings.Contains(path, ".txt") {
				fmt.Println(path)
				// ler linha e manda
				init := time.Now()
				err := lerLinhaPublish(path, TypeData, addr)
				start := time.Now()
				diff := start.Sub(init).Milliseconds()
				fmt.Println("diff: .........", diff)
				if err != nil {
					log.Println(err)
				}
			}
			return nil
		})
	return err
}
func TrataTopic(types string) string {
	var valor string
	switch types {
	case "ABP":
		valor = "abp"
		break
	case "PULSE":
		valor = "pulse"
		break
	case "SpO2":
		valor = "spO2"
		break
	}
	return valor
}

func TrataData(text string, types string) string {
	valor := strings.Replace(text, types, "", -1)
	switch types {
	case "ABP":
		valor = strings.Replace(valor, "\t", " ", -1)
		valor = strings.TrimSpace(valor)
		break
	case "PULSE":
		valor = strings.Replace(valor, "\t", " ", -1)
		valor = strings.TrimSpace(valor)
		break
	case "SpO2":
		valor = strings.Replace(valor, "\t", " ", -1)
		valor = strings.TrimSpace(valor)
		break
	}
	return valor
}

func SendData(valor string, addr string, topic string) {
	fmt.Println("Valor:", valor , "tamanho :", int64(len(valor)));
	limiter := time.Tick(500*1000000)
	<-limiter
	stri := `{"topic" : "` + TrataTopic(topic) + `", "payload": "` + valor + `", "rate": 400}`
	fmt.Println("Dados json", stri)
	err := ag.Post(
		addr,
		[]byte(stri),
	)
	start := time.Now()
	fmt.Println("Sensor send", start, time.Millisecond * 500)
	if err != nil {
		fmt.Println("error send data sensor: ", err)
	}
}

//var pasta int = 37001

//var zero string = "0"
// concatena := fmt.Sprintf("%s%d", "0", "pasta")

//fmt.Println(concatena)

// Funcao que le o conteudo do arquivo e retorna um slice the string com todas as linhas do arquivo
func lerTexto(oi string) ([]string, error) {
	//abre o arquivo

	arquivo, err := os.Open(oi)
	// Caso tenha encontrado algum erro ao tentar abrir o arquivo retorne o erro encontrado
	if err != nil {
		return nil, err
	}

	// Garante que o arquivo sera fechado apos o uso
	defer arquivo.Close()

	// Cria um scanner que le cada linha do arquivo
	var linhas []string
	scanner := bufio.NewScanner(arquivo)
	for scanner.Scan() {
		linhas = append(linhas, scanner.Text())
	}

	// Retorna as linhas lidas e um erro se ocorrer algum erro no scanner
	return linhas, scanner.Err()
}

func WriteToFile(filename string, datas []string) error {

	file, err := os.OpenFile(filename, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0644)

	if err != nil {
		log.Fatalf("failed creating file: %s", err)
		return err
	}

	datawriter := bufio.NewWriter(file)

	for _, data := range datas {
		_, _ = datawriter.WriteString(data + "\n")
	}

	datawriter.Flush()
	file.Close()
	return file.Sync()
}

func HandleDataAbp() []*client.PublisherType {

	var abp []string
	abp, err := lerlinha("./dados/03700001.txt", "ABP")

	if err != nil {
		log.Fatal("leitura error:", err)
	}

	publist := []*client.PublisherType{}

	for _, linha := range abp {

		pub := new(client.PublisherType)
		valor := strings.Replace(linha, "ABP", "", -1)
		valor = strings.Replace(valor, "\t", " ", -1)
		valor = strings.TrimSpace(valor)
		stri := `{"topic" : "abp", "payload": "` + valor + `", "rate": 400}`
		//	fmt.Println(stri)
		err := json.Unmarshal([]byte(stri), &pub)
		if err != nil {
			log.Fatal("json error: ", err)
		}
		publist = append(publist, pub)
	}
	return publist
}

func HandleDataSp02() []*client.PublisherType {

	publist := []*client.PublisherType{}
	var spO2 []string
	spO2, err := lerlinha("./dados/03700001.txt", "SpO2")

	if err != nil {
		log.Fatal("leitura error: ", err)
	}

	for _, linha := range spO2 {

		pub := new(client.PublisherType)
		valor := strings.Replace(linha, "SpO2", "", -1)
		valor = strings.Replace(valor, "\t", " ", -1)
		valor = strings.TrimSpace(valor)
		fmt.Println(valor)
		var stri = `{"topic" : "sp02", "payload": "` + valor + `", "rate": 400}`
		//	fmt.Println(stri)

		err := json.Unmarshal([]byte(stri), &pub)
		if err != nil {
			log.Fatal("json error: ", err)
		}
		//	fmt.Println(pub)

		publist = append(publist, pub)
	}

	return publist
}

///func main() {
//	var abp []string
//	abp, err := lerlinha("../dados/03700001.txt", "ABP")

//	if err != nil {
//		log.Fatalf("Erro:", err)
//	}

//err2 := WriteToFile("abp2.txt", abp)
//if err2 != nil {
//log.Fatalf("Erro:", err)
//}
//	publist := []*client.PublisherType{}

//	for indice, linha := range abp {
//		fmt.Println(indice, linha)

//		pub := new(client.PublisherType)
//		pub.Topic = "ABP"
//		fmt.Println("lunha " + linha)
//		var valor = strings.Replace(linha, "ABP", " ", -1)
//		fmt.Println(json.RawMessage(valor))
//		pub.Payload = json.RawMessage(valor)
//		pub.Rate = 50
//		publist = append(publist, pub)
//		fmt.Println(pub)
//	}
//	fmt.Println(publist)

//	for i := range publist {
//	publist := publist[i]
//	fmt.Println("Location:", publist)
//}
//	var sp02 []string

//	sp02, err3 := lerlinha("../dados/physionet.org/files/mimicdb/1.0.0/037/03700032.txt", "Sp")
//	if err3 != nil {
///		log.Fatalf("Erro:", err3)
//	}

//	err4 := WriteToFile("sp02.txt", sp02)
//	if err4 != nil {
//
//		//log.Fatalf("Erro:", err)
//	}
//	var pulse []string
//	pulse, err5 := lerlinha("../dados/physionet.org/files/mimicdb/1.0.0/037/03700031.txt", "PULSE")
//	if err5 != nil {
//		log.Fatalf("Erro:", err5)
//
//	}

//	err6 := WriteToFile("pulse2.txt", pulse)
//	if err6 != nil {

//log.Fatalf("Erro:", err)
//	}

//for indice, linha := range abp {
//fmt.Println(abp.txt)
//}
//for indice, linha := range sp02 {
//fmt.Println(indice, linha)
//}
//for indice, linha := range pulse {
//fmt.Println(indice, linha)
//}

//}
