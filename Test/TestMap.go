package main

import "fmt"

// QOS effort
type QOS int64

type agBufferState struct {
	AgID string
	QOS1 int64
	QOS2 int64
	QOS3 int64
}

// Effort values
type Effort []int64

type agSubscriber struct {
	Host string
	QOS  QOS
}

type agSubscription struct {
	AgID            string
	EffortReturnURL string
}

var aggregatorBufferSize = map[string]agBufferState{}
var subscribedAggregators = map[string]agSubscription{}
var aggregatorSubscribers = map[string][]agSubscriber{}

func main() {

	ag1 := agSubscription{
		AgID:            "ag123",
		EffortReturnURL: "localhost",
	}
	ag2 := agSubscription{
		AgID:            "ag456",
		EffortReturnURL: "localhost",
	}
	ag3 := agSubscription{
		AgID:            "ag789",
		EffortReturnURL: "localhost",
	}

	c1 := agSubscriber{
		Host: "localhost",
		QOS:  1024,
	}
	c2 := agSubscriber{
		Host: "localhost",
		QOS:  2048,
	}
	c3 := agSubscriber{
		Host: "localhost",
		QOS:  3076,
	}

	ag123c1 := []agSubscriber{}
	ag456c2 := []agSubscriber{}
	ag789c3 := []agSubscriber{}

	ag123c1 = append(ag123c1, c1)
	ag456c2 = append(ag456c2, c2)
	ag789c3 = append(ag789c3, c3)

	b1 := agBufferState{
		AgID: "ag123",
		QOS1: 0,
		QOS2: 0,
		QOS3: 2,
	}

	b2 := agBufferState{
		AgID: "ag456",
		QOS1: 0,
		QOS2: 2,
		QOS3: 1,
	}

	b3 := agBufferState{
		AgID: "ag789",
		QOS1: 1,
		QOS2: 1,
		QOS3: 2,
	}

	subscribedAggregators = map[string]agSubscription{
		"ag123": ag1,
		"ag456": ag2,
		"ag789": ag3,
	}
	aggregatorSubscribers = map[string][]agSubscriber{
		"ag123": ag123c1,
		"ag456": ag456c2,
		"ag789": ag789c3,
	}
	aggregatorBufferSize = map[string]agBufferState{
		"ag123": b1,
		"ag456": b2,
		"ag789": b3,
	}

	cal := int64(0)
	total := int64(125000) //
	for a, b := range aggregatorBufferSize {

		fmt.Println("a:", a, "b:", b)
		cal = cal + ((b.QOS1 * 1) + (b.QOS2 * 2) + (b.QOS3 * 3))
		// cal = 3
		//cal = 3 + 2
		// cal = 5 + 1
		// cal = 6
	}
	valoDeI := int64(total / cal)
	fmt.Println("valor: ", valoDeI, cal)
	i := int64(0)
	s := int64(0)
	p := int64(0)
	for k, b := range aggregatorBufferSize {

		//	if b.QOS1 < 1 {
		//		i = 1 * valoDeI
		//	} else {
		//		i = b.QOS1 * valoDeI
		//	}
		//	if b.QOS2 < 1 {
		//		s = 1 * valoDeI
		//	} else {
		//		s = b.QOS2 * (valoDeI * 2)
		//	}
		//	if b.QOS3 < 1 {
		//		p = 1 * valoDeI
		//	} else {
		//		p = b.QOS3 * (valoDeI * 3)
		//	}
		i = b.QOS1 * valoDeI
		s = b.QOS2 * (valoDeI * 2)
		p = b.QOS3 * (valoDeI * 3)
		fmt.Println("a", k, "filas: ", i, s, p)
	}

}
