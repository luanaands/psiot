package client

type pubber func(topic string, payload string)

// PublisherType defines topic, payload and rate
type PublisherType struct {
	Topic   string `json:"topic"`
	Payload string `json:"payload"`
	Rate    int    `json:"rate"`
}

// Publisher periodically sends a topics payload
func Publisher(t PublisherType, p pubber) {
	//	fmt.Println(t.Payload)

	p(t.Topic, t.Payload)

}
